module.exports = {
  routes: () => {
    const controllers = require('./geocode.controllers')();
    const geocodeModels = require('./geocode.models');
    return [
      {
        method: 'GET',
        path: '/geocode',
        handler: controllers.getJSONGeocode,
        config: {
          description: 'Get geocode',
          notes: 'Get geocode for an inputted search query',
          tags: ['api', 'Geocode'],
          validate: {
            query: geocodeModels.apiFilterQuery,
          }
        }
      },
      {
        method: 'GET',
        path: '/geocode/suggestions',
        handler: controllers.getGeocodeSuggestions,
        config: {
          description: 'Get Allegheny geocode',
          notes: 'Get allegheny geocode for an inputted search query',
          tags: ['api', 'Geocode'],
          validate: {
            query: geocodeModels.suggestionsQuery,
          }
        }
      },
      // {
      //   method: 'GET',
      //   path: '/geocode/inspect',
      //   handler: controllers.inspectJSON,
      //   config: {
      //     description: 'Inspect geoJSON',
      //     notes: 'Inspect geoJSON file for structure/contents',
      //     tags: ['api', 'Geocode'],
      //   }
      // },
      // {
      //   method: 'GET',
      //   path: '/geocode/validate',
      //   handler: controllers.validateJSON,
      //   config: {
      //     description: 'Validate geoJSON',
      //     notes: 'Inspect geoJSON file for structure/contents',
      //     tags: ['api', 'Geocode'],
      //   }
      // },
    ];
  },
};
