module.exports = () => {
  // const Boom = require('@hapi/boom');
  const Parser = require('parse-address');
  const Parcels = require('./address-points.json');
  const pghNhbds = require('./pittsburgh-neighborhoods.json');
  const inside = require('point-in-geopolygon');
  console.log('parcels loaded');
//   const used = process.memoryUsage().heapUsed / 1024 / 1024;
// console.log(`The script uses approximately ${Math.round(used * 100) / 100} MB`);
  return {
    getGeocode: async function(request, h) {
      let search = request.query.q;
      let matches = await processStream(search);
      return h.response({results: matches});
    },
    getJSONGeocode: async function(request, h) {
      console.time('getJSONGeocode');
      let search = request.query.q;
      let matches = processRequiredJSON(search);
      for (let match of matches) {
        // Layer neighborhood data in for Pittsburgh
        if (match.components.city == "Pittsburgh") {
          let point = [match.geometry.lng, match.geometry.lat];
          let feature = inside.feature(pghNhbds, point);
          if (feature !== -1) {
            match.components.suburb = feature.properties.hood;
          }
        }
      }
      console.timeEnd('getJSONGeocode');
      return h.response({results: matches});
    },
    getGeocodeSuggestions: async function(request, h) {
      let search = request.query.q;
      let response = await getSuggestions(search);
      let suggestions = response.suggestions;
      return h.response(suggestions);
    },
    inspectJSON: async function(request, h) {
      let inspection = {};
      let parcels = Parcels.features;
      inspection.length = parcels.length;
      inspection.features = {};
      for (let i = 0; i < parcels.length; i++) {
        for (let key in parcels[i].properties) {
          let value = parcels[i].properties[key];
          if (!inspection.features.hasOwnProperty(key)) {
            inspection.features[key] = {};
          }
          switch (key) {
            case 'ADDRESS_TYPE':
            case 'ADDRESS_TY':
            case 'STATUS':
            case 'ADDR_NUM_PREFIX':
            case 'ADDR_NUM_SUFFIX':
            case 'ADDR_NUM_S':
            case 'ST_PREFIX':
            case 'ST_PRETYPE':
            case 'ST_PREMODIFIER':
            case 'ST_POSTMODIFIER':
            case 'MUNICIPALITY':
            case 'MUNICIPALI':
              inspection.features[key][value] = true;
              break;
            case 'COUNTY':
              inspection.features[key][value] = true;
              break;
            case 'STATE':
              inspection.features[key][value] = true;
              break;
            case 'ST_SUFFIX':
              inspection.features[key][value] = true;
              break;
            default:
              // do nothing
          }
        }
      }
      return h.response(inspection);
    },
    validateJSON: async function(request, h) {
      let keyTarget = "STATUS";
      let valueTarget = "Disputed";
      let someCount = 5;
      let matches = [];
      let parcels = Parcels.features;
      for (let i = 0; i < parcels.length; i++) {
        let parcel = parcels[i];
        let invalidReasons = [];
        if (!parcel.hasOwnProperty('geometry') || parcel.geometry === null) {
          invalidReasons.push('missing geometry');
        } else {
          if (!parcel.geometry.hasOwnProperty('coordinates')) {
            invalidReasons.push('missing coordinates');
          } else {
            if (parcel.geometry.coordinates.length !== 2) {
              invalidReasons.push('not two coordinates');
            }
          }
        }

        if (invalidReasons.length !== 0) {
          matches.push({
            invalidReasons: invalidReasons,
            feature: parcel
          });
        }
        // for (let key in parcels[i].properties) {
        //   let value = parcels[i].properties[key];
        //   if (key == keyTarget && value == valueTarget) {
        //     matches.push(parcels[i]);
        //   }
        // }
        // if (matches.length >= someCount) {
        //   break;
        // }
      }
      console.log('matches length', matches.length)
      return h.response(matches);
    },
    getSome: async function(request, h) {
      let keyTarget = "STATUS";
      let valueTarget = "Disputed";
      let someCount = 5;
      let matches = [];
      let parcels = Parcels.features;
      for (let i = 0; i < parcels.length; i++) {
        for (let key in parcels[i].properties) {
          let value = parcels[i].properties[key];
          if (key == keyTarget && value == valueTarget) {
            matches.push(parcels[i]);
          }
        }
        if (matches.length >= someCount) {
          break;
        }
      }
      return h.response(matches);
    },
  };

  function getSuggestions(search) {
    let api = new URL('https://gisdata.alleghenycounty.us/arcgis/rest/services/Geocoders/Composite/GeocodeServer/suggest');
    api.searchParams.set('f', 'json');
    api.searchParams.set('maxSuggestions', '6');
    api.searchParams.set('text', search);

    return new Promise((resolve, reject) => {
      const https = require('https');
      https.get(api, res => {
        res.setEncoding("utf8");
        let body = "";
        res.on("data", data => {
          body += data;
        });
        res.on("end", () => {
          body = JSON.parse(body);
          resolve(body);
        });
        res.on('error', error => {
          reject(error);
        });
      });

    });
  }

  function getAddressCandidates(suggestion) {
    let api = new URL('https://gisdata.alleghenycounty.us/arcgis/rest/services/Geocoders/Composite/GeocodeServer/findAddressCandidates');
    api.searchParams.set('f', 'json');
    api.searchParams.set('outSR', '{"wkid"%3A102100}');
    api.searchParams.set('outFields', '*');
    api.searchParams.set('maxLocations', '6');
    api.searchParams.set('singleLine', suggestion.text);
    api.searchParams.set('magicKey', suggestion.magicKey);
    return new Promise((resolve, reject) => {
      const https = require('https');
      https.get(api, res => {
        res.setEncoding("utf8");
        let body = "";
        res.on("data", data => {
          body += data;
        });
        res.on("end", () => {
          body = JSON.parse(body);
          resolve(body);
        });
        res.on('error', error => {
          reject(error);
        });
      });

    });
  }

  function processStream(search) {
    search = search.toUpperCase();
    let parsed = Parser.parseLocation(search);
    let confidenceFloor = 3;
    let confidenceScore = 0.5;
    if (!parsed) {

    }
    if (Object.keys(parsed).length > 6) {
      confidenceScore = 0.7;
      confidenceFloor = 5;
    }
    let matches = [];
    return new Promise((resolve, reject) => {
      const fs = require('fs');
      const JSONStream = require('JSONStream');
      const es = require("event-stream");
      var getStream = function () {
        const jsonData = './lib/geocode/address-points.json';
        const readOpts = {
          highWaterMark: Math.pow(2,8),
          encoding: 'utf8',
        };
        const stream = fs.createReadStream(jsonData, readOpts);
        const parser = JSONStream.parse('features.*');
        return stream.pipe(parser);
      };
      try {
        getStream()
        .pipe(es.through(
          function(parcel) {
            this.pause();
            let confidence = 0;
            for (let key of Object.keys(parsed)) {
              let property = parcel.properties;
              let value = parsed[key];
              value = value.toUpperCase();
              switch (key) {
                case 'number':
                  if (property.ADDR_NUM == value) {
                    confidence++;
                  }
                  break;
                case 'prefix':
                  if (property.ST_PREFIX == value) {
                    confidence++;
                  }
                  break;
                case 'street':
                  if (property.ST_NAME == value) {
                    confidence++;
                  }
                  break;
                case 'type':
                  if (property.ST_TYPE == value) {
                    confidence++;
                  }
                  break;
                case 'city':
                  if (property.MUNICIPALI == value) {
                    confidence++;
                  }
                  break;
                case 'state':
                  if (property.STATE == value) {
                    confidence++;
                  }
                  break;
                case 'zip':
                  if (property.ZIP_CODE == value) {
                    confidence++;
                  }
                  break;
                default:
                  // do nothing
              }
            }
            if (
              confidence >= confidenceFloor
            ) {
              confidenceFloor = confidence;
              matches.push({
                confidence: confidence,
                geometry: {
                  lat: parcel.geometry.coordinates[1],
                  lng: parcel.geometry.coordinates[0],
                },
                components: formatPropertiesComponents(parcel.properties),
              });
            }
            this.resume();
            return parcel;
          },
          function end() {
            console.log("stream reading ended");
            this.emit("end");
            matches.sort((a, b) => {
              if (a.confidence > b.confidence) {
                return -1;
              } else if (a.confidence < b.confidence) {
                return 1;
              }
              return 0;
            });

            let maxReturnLength = 5;
            resolve(matches.slice(0, maxReturnLength));
          }
        ));
      } catch (e) {
        console.log('error', e);
        reject(e);
      }
    });
  }

  function processRequiredJSON(search) {
    search = search.toUpperCase();
    let parsed = Parser.parseLocation(search);
    let confidenceFloor = 3;
    let confidenceScore = 0.5;
    // if we didn't get back something from the parser, bail
    if (!parsed) {
      console.error('unsuccessful parse. search, parsed', search, parsed);
      return [];
    }
    if (Object.keys(parsed).length > 6) {
      confidenceScore = 0.7;
      confidenceFloor = 5;
    }

    let matches = [];
    let parcels = Parcels.features;
    for (let i = 0; i < parcels.length; i++) {
      let parcel = parcels[i];
      let confidence = 0;
      if (parcel.geometry === null) {
        // don't bother checking parcels without coords
        continue;
      }

      for (let key of Object.keys(parsed)) {
        let property = parcel.properties;
        let value = parsed[key];
        value = value.toUpperCase();
        switch (key) {
          case 'number':
            if (property.ADDR_NUM == value) {
              confidence = confidence + 1.1;
            }
            break;
          case 'prefix':
            if (property.ST_PREFIX == value) {
              confidence++;
            }
            break;
          case 'street':
            // convert things like FIFTH to 5TH
            value = ordsConvert(value);
            // Need to do a number convert comparison. like First to 1st, Second to 2nd, Fifth to 5th, etc
            if (property.ST_NAME == value) {
              confidence = confidence + 1.1;
            } else if (
              parsed.hasOwnProperty('prefix') &&
              property.ST_PREFIX == null
            ) {
              // Some streets like 'West Liberty' get parsed like they have a prefix, but the county considers it all the street name
              let combined = abbrConvert(parsed.prefix) + ' ' + parsed.street;
              if (combined == property.ST_NAME) {
                confidence = confidence + 1.1;
              }
            }
            break;
          case 'type':
            if (property.ST_TYPE == value) {
              confidence++;
            }
            break;
          case 'city':
            if (
              property.MUNICIPALI == value ||
              processMunicipality(property.MUNICIPALITY) == value
            ) {
              confidence++;
            }
            break;
          case 'state':
            if (property.STATE == value) {
              confidence++;
            }
            break;
          case 'zip':
            if (property.ZIP_CODE == value) {
              confidence++;
            }
            break;
          default:
            // do nothing
        }
      }
      if (
        confidence >= confidenceFloor
      ) {
        confidenceFloor = confidence;
        matches.push({
          confidence: confidence,
          geometry: {
            lat: parcel.geometry.coordinates[1],
            lng: parcel.geometry.coordinates[0],
          },
          components: formatPropertiesComponents(parcel.properties),
        });
      }

    }

    matches.sort((a, b) => {
      if (a.confidence > b.confidence) {
        return -1;
      } else if (a.confidence < b.confidence) {
        return 1;
      }
      return 0;
    });

    let maxReturnLength = 5;
    return matches.slice(0, maxReturnLength);
  }

  function termConvert(term) {
    const TERMS = require('./terms.js').TERMS;
    if (!TERMS.hasOwnProperty(term)) {
      return term;
    }
    return TERMS[term];
  }

  function formatPropertiesComponents(properties) {
    let components = {};
    let street = '';
    let possibleParts = [
      'ADDR_NUM',
      'ST_PREFIX',
      'ST_NAME',
      'ST_TYPE',
      'MUNICIPALI',
      'MUNICIPALITY',
      'COUNTY',
      'STATE',
      'ZIP_CODE',
    ];
    for (let part of possibleParts) {
      if (!properties.hasOwnProperty(part)) {
        continue;
      }
      if (properties[part] === null) {
        continue;
      }
      let value = properties[part];
      if (part !== 'STATE') {
        value = abbrConvert(value);
        value = capitalize(value);
      }

      switch (part) {
        case 'ADDR_NUM':
          components.house_number = value;
          break;
        case 'ST_PREFIX':
          street += value + ' ';
          break;
        case 'ST_NAME':
          street += value + ' ';
          break;
        case 'ST_TYPE':
          street += value + ' ';
          break;
        case 'MUNICIPALI':
          components.city = value;
          break;
        case 'MUNICIPALITY':
          components.city = processMunicipality(value);
          break;
        case 'COUNTY':
          if (!value.endsWith('County')) {
            value = value + ' County';
          }
          components.county = value;
          break;
        case 'STATE':
          components.state_code = value;
          break;
        case 'ZIP_CODE':
          components.postcode = value;
          break;
        default:
          // do nothing
      }

    }
    components.road = street.trim();
    components['ISO_3166-1_alpha-2'] = 'US';
    return components;
  }

  function capitalize(string) {
    return string.toLowerCase()
    .split(' ')
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(' ');
  }

  function abbrConvert(string) {
    const ABBRS = require('./terms.js').ABBRS;
    return string.split(' ')
    .map((s) => {
      if (!ABBRS.hasOwnProperty(s)) {
        return s;
      }
      return ABBRS[s];
    })
    .join(' ');
  }

  function ordsConvert(string) {
    const ORDS = require('./terms.js').ORDS;
    return string.split(' ')
    .map((s) => {
      if (!ORDS.hasOwnProperty(s)) {
        return s;
      }
      return ORDS[s];
    })
    .join(' ');
  }

  function processMunicipality(string) {
    // original string
    let oString = string;
    string = string.toUpperCase();
    if (
      string.startsWith('BALDWIN') ||
      string.startsWith('ELIZABETH') ||
      string.startsWith('SPRINGDALE')
    ) {
      // don't modify these because they need their muni types
      return oString;
    }

    if (string.endsWith(' TOWNSHIP')) {
      return oString.substring(0, oString.length - 9);
    } else if (string.endsWith(' BOROUGH')) {
      return oString.substring(0, oString.length - 8);
    } else if (string.endsWith(' MUNICIPALITY')) {
      return oString.substring(0, oString.length - 13);
    }
    return oString;
  }

};

function loadJson(url) {
  let https = require('https');
  return new Promise((resolve, reject) => {

    https.get(url, res => {
      res.setEncoding("utf8");
      let body = "";
      res.on("data", data => {
        body += data;
      });
      res.on("end", () => {
        body = JSON.parse(body);
        resolve(body);
      });
      res.on('error', error => {
        reject(error);
      });
    });

  });
}
