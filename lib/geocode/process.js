const Source = require('./address-points_source.json');
console.log('processJSON called');
let invalidCount = 0;
let matches = [];
let parcels = Source.features;
for (let i = 0; i < parcels.length; i++) {
  let parcel = parcels[i];
  let invalidReasons = [];
  if (!parcel.hasOwnProperty('geometry') || parcel.geometry === null) {
    invalidReasons.push('missing geometry');
  } else {
    if (!parcel.geometry.hasOwnProperty('coordinates')) {
      invalidReasons.push('missing coordinates');
    } else {
      if (parcel.geometry.coordinates.length !== 2) {
        invalidReasons.push('not two coordinates');
      }
    }
  }
  if (parcel.properties["COUNTY"] !== "ALLEGHENY COUNTY") {
    invalidReasons.push('wrong county');
  }

  if (invalidReasons.length > 0) {
    // console.log('invalid');
    invalidCount++;
    Source.features.splice(i,1);
    matches.push({
      invalidReasons: invalidReasons,
      feature: parcel
    });
  } else {
    // console.log('valid');
    // we can clean up data more from where it started
    // Trim properties values
    try {
      Source.features[i].properties["ADDR_NUM"] = Source.features[i].properties["ADDR_NUM"].trim();
      Source.features[i].properties["ST_NAME"] = Source.features[i].properties["ST_NAME"].trim();
      if (Source.features[i].properties["ST_TYPE"] !== null) {
        Source.features[i].properties["ST_TYPE"] = Source.features[i].properties["ST_TYPE"].trim();
      }
      Source.features[i].properties["MUNICIPALITY"] = Source.features[i].properties["MUNICIPALITY"].trim();
      Source.features[i].properties["COUNTY"] = Source.features[i].properties["COUNTY"].trim();
      Source.features[i].properties["STATE"] = Source.features[i].properties["STATE"].trim();
      if (Source.features[i].properties["ZIP_CODE"] !== null) {
        Source.features[i].properties["ZIP_CODE"] = Source.features[i].properties["ZIP_CODE"].trim();
      }
    } catch (e) {
      console.log('Error with trimming properties:', e);
    }

    // Remove properties we don't use
    delete Source.features[i].properties["FULL_ADDRESS"];
    delete Source.features[i].properties["OBJECTID"];
    delete Source.features[i].properties["FEATURE_KEY"];
    delete Source.features[i].properties["ADDRESS_ID"];
    delete Source.features[i].properties["PARENT_ID"];
    delete Source.features[i].properties["STREET_ID"];
    delete Source.features[i].properties["DUP_STREET_ID"];
    delete Source.features[i].properties["ADDRESS_TYPE"];
    delete Source.features[i].properties["STATUS"];
    delete Source.features[i].properties["ST_PREMODIFIER"];
    delete Source.features[i].properties["ADDR_NUM_PREFIX"];
    delete Source.features[i].properties["ADDR_NUM_SUFFIX"];
    delete Source.features[i].properties["ST_PRETYPE"];
    delete Source.features[i].properties["ST_POSTMODIFIER"];
    delete Source.features[i].properties["UNIT"];
    delete Source.features[i].properties["GlobalID"];
    delete Source.features[i].properties["UNIT_TYPE"];
    delete Source.features[i].properties["EXP_FLAG"];
    delete Source.features[i].properties["EDIT_USER"];
    delete Source.features[i].properties["EDIT_DATE"];
    delete Source.features[i].properties["ENFORCE_VALIDATION"];
    delete Source.features[i].properties["COMMENT"];
    delete Source.features[i].properties["ZIP_CODE4"];
    delete Source.features[i].properties["SOURCE_ID"];
    delete Source.features[i].properties["SOURCE"];
    delete Source.features[i].properties["FLOOR"];
  }

}
const Merge = require('./address-points_merge.json');
Merge.forEach(mergeItem => {
  Source.features.push(mergeItem);
})
var outJSON = JSON.stringify(Source);
const fs = require('fs');
fs.writeFileSync('./lib/geocode/address-points.json', outJSON);
console.log("objectsRemoved:", invalidCount);
console.log("invalidObjects:", matches);
