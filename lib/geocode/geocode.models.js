const Joi = require('@hapi/joi');

module.exports = {
  apiFilterQuery: Joi.object().keys({
    q: Joi.string().required(),
    key: Joi.string(),
    bounds: Joi.string(),
  }),
  suggestionsQuery: Joi.object().keys({
    q: Joi.string().required(),
  })
};
